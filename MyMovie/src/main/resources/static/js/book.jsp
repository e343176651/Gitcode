<%@ page contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<html>
<head>
<meta charset="UTF-8">
<title>座位预定</title>
<link href="css/book.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-3.1.1.min.js"></script>
<script>
$(document).ready(function(){
	var seat_num ;
	var total_bill      = 0 ;
	var pricePerTicked  = 50;//单价
	var maximumSeats    = 4;//预定座位数目的最大限制
	
	$('#bookNowButton').hide(); // 隐藏预定按钮
    $('.seat').each(function() {       
   		var column_num = parseInt( $(this).index() ) + 1;
    	var row_num = parseInt( $(this).parent().index() )+1;    
    	seat_num = row_num+"-"+column_num ;  
   		$(this).text(seat_num); // 座位号
   		$(this).addClass("seat"+seat_num);  // 个座位加css
    });
    
	$("#seats .seat").click(function() {  
	    $('#errMsg').html('');
	   
	    if($(this).hasClass('select')){ // 检查是否被选中
		    $(this).removeClass('select'); //如果选中了，移除选中的css
		    $(this).css('background-color','#D8D8D8'); // 重新加个背景
		    var currentSeatClass = $(this).attr('class').split(' ')[1]; 
		    console.log(currentSeatClass);
		    $( "#selected_seat ."+currentSeatClass ).remove();
	    }else if($(".your_selected_seat").length<maximumSeats && !$(this).hasClass('select')){ // 检查预定的座位数目是否超出限制
	   
	    	$(this).css('background-color','#71DCAA'); // 加背景颜色
	    	$(this).addClass("select"); // 添加选中css
	    	var column_num = parseInt( $(this).index() ) + 1;
	    	var row_num = parseInt( $(this).parent().index() )+1;    
	    	$( "#selected_seat" ).append("<span class='your_selected_seat seat"+row_num+"-"+column_num+" '> 座位号: <b style='color:#EAABFF'>" + row_num+"-"+column_num +"</b> </br></span>");
            
	    }else {
	    	$('#errMsg').html(function(){
	    		var erro = "单次购买不能超过4张哟";
	    		alert(erro);
	    	});                
	    }
	    if($(".your_selected_seat").length){
	    	$('#bookNowButton').fadeIn(1000);       
	    }else {              
	    	$('#bookNowButton').fadeOut(1000);                        
	    }                   
	    //计算总价                        
	    total_bill = $(".your_selected_seat").length * pricePerTicked*(1-0.3);                        
	    $('#total > span').html(total_bill);                
	});    
	//}
});
</script>
<body>      
<!--BOOKING SECTION START-->
<form action="index.html" method="post">
	<div style="width: 900px; margin: 0 auto;">
		<span id='screen'>
			<p>屏幕</p>
		</span>
		
		<div id="seats">
			<div class="seatsRaw">
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
			</div>
			<div class="seatsRaw">
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
			</div>
			<div class="seatsRaw">
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
			</div>

			<div class="seatsRaw">
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
			</div>

			<div class="seatsRaw">
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
			</div>

			<div class="seatsRaw">
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
			</div>
			<div class="seatsRaw">
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
			</div>
			<div class="seatsRaw">
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
				<div class="seat"></div>
			</div>
		</div>
		
		<div id="booking_desc">
			<div class="booking_left">
				<p style="color: #FBBC53; font-weight: bold; font-size: larger;">您选中的座位
				</p>
				<div id="selected_seat"></div>
				<br>
				<input id="bookNowButton" type="submit" value="确定" onclick=onclick="window.location.href='webapp/index.html'"/>
				<script>
				$(function(){ $('#bookNowButton').click(function(){ alert('订票成功'); }) })
				</script>
				<div id="errMsg"></div>
			</div>
			<div class="booking_right">
				票价: 50.00 元/张 <br>
				<br>
				<div id="total">
					总价：<span> 0 </span>元
				</div>
			</div>
		</div>
	</div>
</form>
</body>
</html>>