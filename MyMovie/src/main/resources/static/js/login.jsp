<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<html>
<head>
<meta charset="UTF-8">
<title>叮叮电影网</title>
<link rel="stylesheet" href="css/login.css">
<script type="text/javascript" src="../js/login.js"></script>
</head>
<body>
	<header>
		<a href="#"><img src="images/logo.png"></a> <span>&nbsp;&nbsp;欢迎登录</span>
	</header>

	<div class="form-panel">
		<form action="login.do" onsubmit="return checkUser()+checkPwd()==2">
			<h2>
				登陆叮叮电影网 <a href="regist.jsp">新用户注册</a>
			</h2>
			<div class="field">
				<input type="text" placeholder="请输入用户名" id="user"
					onblur="checkUser();"> 
			</div>
			<div class="field">
				<input type="password" placeholder="请输入密码" id="pwd"
					onblur="checkPwd();" style="width: 154px; "> 
					<% String msg = (String)request.getAttribute("login_falied"); %>
					<span id="msg_pwd" style="color:red;font-size:24px;">
						<%=msg == null?"":msg%>
					</span>
					
			</div>
			<div class="checkbox">
				<input type="checkbox" id="save"> <label for="save">自动登录</label>
				<a href="#">忘记密码?</a>
			</div>
			<div class="btn">
				<input type="submit" value="登录">
			</div>
		</form>
	</div>
</body>
</html>







