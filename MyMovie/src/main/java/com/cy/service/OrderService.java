package com.cy.service;

import com.cy.entity.Order;
import com.cy.vo.PageObject;

public interface OrderService {
	
	PageObject<Order> findOrder(Integer uid, Integer pageCurrent);

}
