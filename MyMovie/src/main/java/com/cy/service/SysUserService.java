package com.cy.service;

import java.util.List;

import com.cy.entity.SysUser;
import com.cy.vo.PageObject;

public interface SysUserService {
	List<SysUser> findAllPageObjects();
	
	PageObject<SysUser> findPageObjects(
			String username,
			Integer pageCurrent);
	
	int updatePassword(String password);
	
	int registUser(SysUser sysUser);
}
