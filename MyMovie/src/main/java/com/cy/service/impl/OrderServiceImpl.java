package com.cy.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.dao.OrderDao;
import com.cy.entity.Order;
import com.cy.exception.ServiceException;
import com.cy.service.OrderService;
import com.cy.vo.PageObject;

@Service
public class OrderServiceImpl implements OrderService{

	@Autowired
	private OrderDao orderDao;
	
	@Override
	public PageObject<Order> findOrder(Integer uid, Integer pageCurrent) {
//		String tName=Thread.currentThread().getName();
//		System.out.println("user.find.thread.name="+tName);
		//long t1=System.currentTimeMillis();
		//1.参数有效性校验
		if(pageCurrent==null||pageCurrent<1)
			throw new IllegalArgumentException("页码值不正确");
		//2.查询总记录数并校验
		 int rowCount=
		  orderDao.getRowCount(uid);
		 if(rowCount==0)
			   throw new ServiceException("记录不存在");
		//3.查询当前页记录\
		 int pageSize=4;
		 int startIndex=(pageCurrent-1)*pageSize;
		 List<Order> records=
				 orderDao.findOrder(uid,
				 startIndex, pageSize);
		//4.封装查询结果并返回
		PageObject<Order> po=
		new PageObject<>(pageCurrent, pageSize, rowCount, records);
		//long t2=System.currentTimeMillis();
		//System.out.println(t2-t1);
		return po;
	}


}
