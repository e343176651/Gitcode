package com.cy.service.impl;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.util.StringUtils;

import com.cy.dao.UserDao;
import com.cy.pj.common.exception.ServiceException;
import com.cy.pj.entity.User;
import com.cy.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;
	
	/**
	 * 将用户信息保存到数据库
	 */
	
	@Override
	public int saveObject(User entity) {
		//1.验证数据合法性
		if(StringUtils.isEmpty(entity.getUsername()))
		    throw new ServiceException("用户名不能为空");
			if(StringUtils.isEmpty(entity.getPassword()))
			throw new ServiceException("密码不能为空");
		
			//2.将数据写入数据库
			
		
		
			
			int rows=userDao.insertObject(entity);
		   
			//3.返回结果
			return rows;
	
	}

	
	
	@Override
	public User findUser(String username, String password) {

		        //1.验证数据合法性
				if(StringUtils.isEmpty(username))
				    throw new ServiceException("用户名不能为空");
					if(StringUtils.isEmpty(password))
					throw new ServiceException("密码不能为空");
				
					User findUser = userDao.findUser(username, password);
	
		         
		return findUser;
	}

	
	
}
