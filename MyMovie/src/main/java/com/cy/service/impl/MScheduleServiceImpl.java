package com.cy.service.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.dao.MInfoDao;
import com.cy.dao.MScheduleDao;
import com.cy.entity.MInfo;
import com.cy.entity.MSchedule;
import com.cy.exception.ServiceException;
import com.cy.service.MScheduleService;
import com.cy.vo.PageObject;
@Service
public class MScheduleServiceImpl implements MScheduleService{
	
	@Autowired
	private MInfoDao mInfoDao;
	
	@Autowired
	private MScheduleDao mScheduleDao;
	
	@Override
	public PageObject<MSchedule> findPageMSchedule(String mName,Integer pageCurrent) {
		if(pageCurrent==null||pageCurrent<1)
			throw new IllegalArgumentException("页码值不正确");
		//2.查询总记录数并校验
		int rowCount = mScheduleDao.getRowCount(mName);
		if(rowCount==0)
			   throw new ServiceException("记录不存在");
		int pageSize=1;
		int startIndex=(pageCurrent-1)*pageSize;
		List<MSchedule> records = mScheduleDao.findPageMSchedule(mName, startIndex, pageSize);
		return new PageObject(pageCurrent, pageSize, rowCount, records);
	
	}
	@Override
	public int addSchedule(MSchedule mSchedule) {
		Date start;
		Date end;
		Date setend;
		SimpleDateFormat b = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Calendar cal=Calendar.getInstance();
		Calendar cal2=Calendar.getInstance();
		if(mSchedule.getMObj().getId() !=null || mSchedule.getMObj().getId()<=0)
			throw new IllegalArgumentException("请选择电影");
		if(mSchedule.getHallObj().getId()==null||mSchedule.getHallObj().getId()<=0)
			throw new IllegalArgumentException("请选择影厅");
		List<MSchedule> list = mScheduleDao.queryMscheduleList();
		for(int i=0;i<list.size();i++) {
			//对比同影厅电影时间是否冲突
			if(list.get(i).getId()==mSchedule.getHallObj().getId()) {
				//同影厅电影开场时间
				start=list.get(i).getScheduleTime();
				cal.setTime(list.get(i).getScheduleTime());
				cal.add(Calendar.MINUTE, list.get(i).getMObj().getDuration());
				//同影厅散场时间
				end=cal.getTime();
				//添加的电影时间
				MInfo mInfo = mInfoDao.getMinfoById(mSchedule.getMObj().getId());
				cal2.setTime(mSchedule.getScheduleTime());
				cal2.add(Calendar.MINUTE, mInfo.getDuration());
				//添加的电影散场时间
				setend=cal2.getTime();
//					start<setend<end
				if((setend.compareTo(start)>0&&(setend.compareTo(end))<0)) {
					throw new ServiceException("散场时间："+b.format(setend)+"小于"+list.get(i).getMObj().getName()+"的"+
							"散场时间："+b.format(end)+"请重新设置");
//						setend==start
				}else if(setend.compareTo(start)==0) {
					throw new ServiceException("散场时间："+b.format(setend)+"=="+list.get(i).getShowDate()+"的开场时间，请重新设置");
//					setend==end
				}else if((setend.compareTo(end))==0){
					throw new ServiceException("散场时间："+b.format(setend)+"=="+list.get(i).getShowDate()+"的散场时间，请重新设置");
//					strt<scheduleTime<end
				}else if((mSchedule.getScheduleTime().compareTo(start))>0&&(mSchedule.getScheduleTime().compareTo(end))<0) {
					throw new ServiceException("开始时间："+mSchedule.getScheduleTime()+"小于"+list.get(i).getMObj().getName()+"的"+
							"散场时间："+end+"请重新设置");
//					scheduleTime==start
				} else if((mSchedule.getScheduleTime().compareTo(start))==0) {
					throw new ServiceException("开场时间："+b.format(setend)+"=="+list.get(i).getShowDate()+"的开场时间，请重新设置");
//					scheduleTime==end
				} else if((mSchedule.getScheduleTime().compareTo(end))==0) {
					throw new ServiceException("开场时间："+b.format(setend)+"=="+list.get(i).getShowDate()+"的散场时间，请重新设置");
				}
					
				
				
			}
		}
		int rows = mScheduleDao.insertMSchedule(mSchedule);
		return rows;
	}

}
