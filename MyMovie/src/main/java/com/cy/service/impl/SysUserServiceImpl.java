package com.cy.service.impl;

import java.util.List;
import java.util.UUID;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.dao.SysUserDao;
import com.cy.entity.SysUser;
import com.cy.exception.ServiceException;
import com.cy.service.SysUserService;
import com.cy.vo.PageObject;





@Service
public class SysUserServiceImpl implements SysUserService {
	
	@Autowired
	private SysUserDao sysUserDao;
	@Override
	public List<SysUser> findAllPageObjects() {
		List<SysUser> records=sysUserDao.findAllPageObjects();
		return records;
		
		
	}
	@Override
	public PageObject<SysUser> findPageObjects(String username, Integer pageCurrent) {
		if(pageCurrent==null||pageCurrent<1)
			throw new IllegalArgumentException("当前页码值无效");
		int rowCount=sysUserDao.getRowCount(username);
		if(rowCount==0)
			throw new ServiceException("没有找到对应记录");
		int pageSize=3;
		int startIndex=(pageCurrent-1)*pageSize;
		List<SysUser> records=sysUserDao.findPageObjects(username, startIndex, pageSize);
		return new PageObject<>(pageCurrent, pageSize, rowCount, records);
		
	}
	@Override
	public int updatePassword(String password) {
		SysUser user=(SysUser)SecurityUtils.getSubject().getPrincipal();
		String salt=user.getSalt();
		salt=UUID.randomUUID().toString();
		SimpleHash sh=new SimpleHash("MD5",password, salt, 1);
		//String salt=UUID.randomUUID().toString();
		//4.将新密码加密以后的结果更新到数据库
		int rows=sysUserDao.updatePassword(sh.toHex(), salt,user.getId());
		if(rows==0)
		throw new ServiceException("修改失败");
		return rows;
	}
	@Override
	public int registUser(SysUser sysUser) {
		if(sysUser.getUsername()==null)
			throw new IllegalArgumentException("用户名不能为空");
		String reg="^[\\u4e00-\\u9fa5]{1,7}$|^[\\dA-Za-z_]{1,14}$";
		if(sysUser.getUsername()==reg)
			throw new IllegalArgumentException("用户名格式不正确");
		if(sysUser.getPassword()==null)
			throw new IllegalArgumentException("密码不能为空");
		if(sysUser.getPassword().length()<=6)
			throw new IllegalArgumentException("密码至少大于六位");
		if(sysUser.getEmail()==null)
			throw new IllegalArgumentException("邮箱不能为空");
		if(sysUser.getPhone()==null)
			throw new IllegalArgumentException("手机号不能为空");
		int rows=sysUserDao.registUser(sysUser);
		if(rows==0)
			throw new ServiceException("注册失败");
		System.out.println("注册成功");
		return rows;
	}
	}


