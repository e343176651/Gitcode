package com.cy.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.dao.MHallDao;
import com.cy.dao.MInfoDao;
import com.cy.service.MinfoHallService;

@Service
public class MInfoHallServiceImpl implements MinfoHallService{
	
	@Autowired
	private MInfoDao mInfoDao;
	@Autowired
	private MHallDao mHallDao;
	@Override
	public Map<String, List<com.cy.vo.ChecBox>> ChecBox() {
		Map map = new HashMap<String, List<com.cy.vo.ChecBox>>();
		map.put("mInfo", mInfoDao.findMInfo());
		map.put("mHall", mHallDao.findMHall());
		return map;
	}
	
}
