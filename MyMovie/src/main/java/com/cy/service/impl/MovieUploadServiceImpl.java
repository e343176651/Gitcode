package com.cy.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.dao.MovieUploadDao;
import com.cy.entity.Movie;
import com.cy.exception.ServiceException;
import com.cy.service.MovieUploadService;
import com.cy.vo.PageObject;

@Service
public class MovieUploadServiceImpl implements MovieUploadService{

	@Autowired
		MovieUploadDao dao;
	
	@Override
	public int uploadMovie(Movie entity) {
		if(entity==null)
			throw new IllegalArgumentException("保存对象不能为空");
		int rows=0;
		if(entity.getMId()==null) {
		rows=dao.uploadMovie(entity);
		return rows;		
		}
		rows=dao.updateMovie(entity);
		return rows;
	}

	
	@Override
	public PageObject<Movie> findAllMovie(Integer pageCurrent) {
		//1.参数有效性校验
		if(pageCurrent==null||pageCurrent<1)
			throw new IllegalArgumentException("页码值不正确");
		//2.查询总记录数并校验
		 int rowCount=
		   dao.getRowCount();
		 if(rowCount==0)
			   throw new ServiceException("记录不存在");
		//3.查询当前页记录\
		 int pageSize=4;
		 int startIndex=(pageCurrent-1)*pageSize;
		 List<Movie> records=
		 dao.findAllMovie(startIndex, pageSize);
		//4.封装查询结果并返回
		PageObject<Movie> po=
		new PageObject<>(pageCurrent, pageSize, rowCount, records);
		//long t2=System.currentTimeMillis();
		//System.out.println(t2-t1);
		return po;
	}


	@Override
	public Movie findObjectById(Integer mId) {
		Movie movie = dao.findObjectById(mId);
		return movie;
	}


	@Override
	public int deleteMovie(Movie entity) {
		int i = dao.deleteMovie(entity);
		return i;
	}




}
