package com.cy.service.impl;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.cy.exception.ServiceException;
import com.cy.vo.PageObject;
import com.cy.pj.config.PageProperties;
import com.cy.dao.SysAdvDao;
import com.cy.entity.SysAdv;
import com.cy.service.SysAdvService;
@Service
public class SysAdvServiceImpl implements SysAdvService {
	@Autowired
	private SysAdvDao sysAdvDao;
	@Autowired
	private PageProperties pageProperties;
	@Override
	public PageObject<SysAdv> findPageObjects(String name, Integer pageCurrent) {
		//1.验证参数的合法性
		if(pageCurrent==null||pageCurrent<1)
			throw new IllegalArgumentException("页码值不正确");
		//2.查询总记录数
		int rowCount=sysAdvDao.getRowCount(name);
		if(rowCount==0)
			throw new ServiceException("没有对应的记录");

		//3.查询当前页要呈现的记录
		int pageSize=pageProperties.getPageSize();
		System.out.println("pageSize="+pageSize);
		int startIndex=(pageCurrent-1)*pageSize;
		List<SysAdv> records=
				sysAdvDao.findPageObjects(name, startIndex, pageSize);
		//4.封装查询结果
		return new PageObject<>(pageCurrent,pageSize,rowCount,records);
	}
	@Override
	public int deleteObjects(Integer... adviceIds) {
		//1.参数有效性校验
		  if(adviceIds==null||adviceIds.length==0)
		  throw new IllegalArgumentException("请先选择");
		  //2.执行删除操作
		  int rows=sysAdvDao.deleteObjects(adviceIds);
		  //3.判定结果并返回
		  if(rows==0)
		  throw new ServiceException("记录可能已经不存在");
		  return rows;
	}
	@Override
	public int insertObject(SysAdv entity) {
		//1.参数有效性校验
				if(entity==null)
				throw new IllegalArgumentException("保存对象不能为空");
				if(StringUtils.isEmpty(entity.getName()))
					throw new IllegalArgumentException("用户名不能为空");
				if(StringUtils.isEmpty(entity.getEmail()))
					throw new IllegalArgumentException("邮箱不能为空");
				if(StringUtils.isEmpty(entity.getPhone()))
					throw new IllegalArgumentException("电话不能为空");
				if(StringUtils.isEmpty(entity.getName()))
					throw new IllegalArgumentException("建议内容不能为空");
				//2.将对象保存到数据库
				int rows=sysAdvDao.insertObject(entity);
				//3.返回结果
				return rows;
	}

}
