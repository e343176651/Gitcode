package com.cy.service;

import com.cy.vo.PageObject;


import com.cy.entity.SysAdv;

public interface SysAdvService {
	int insertObject(SysAdv entity);
	
	/**
	 * 基于日志记录id执行日志记录删除操作
	 * @param ids
	 * @return
	 */
	int deleteObjects(Integer... adviceIds);
	
	/**
     * 分页查询用户行为日志信息
* @param name (页面上传过来的查询条件)
* @param pageCurrent 当前页码值
* @return 封装了日志记录以及分页信息的对象
*/
PageObject<SysAdv> findPageObjects(
		 String name,
		 Integer pageCurrent);
}
