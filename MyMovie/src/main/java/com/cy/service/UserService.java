package com.cy.service;

import com.cy.pj.entity.User;

public interface UserService {

	/**
	 * 保存用户信息
	 */
	
	int saveObject(User entity);
	
	
	/**
	 * 登录时比对用户名和密码
	 */
	
	User findUser(String username,String password);
	
	
	
	
}
