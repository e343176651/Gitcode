package com.cy.service;


import com.cy.entity.Movie;
import com.cy.vo.PageObject;

public interface MovieUploadService {
		int uploadMovie(Movie entity);
		

		PageObject<Movie> findAllMovie(Integer pageCurrent);
		
		
		Movie findObjectById(Integer mId);
		 
		int deleteMovie(Movie entity);
}
