package com.cy.service;


import com.cy.entity.MSchedule;
import com.cy.vo.PageObject;

public interface MScheduleService {
	/**按条件查询场次*/
	PageObject<MSchedule> findPageMSchedule(String mName,Integer pageCurrent);
	
	/**添加场次*/
	int addSchedule(MSchedule mSchedule);
	
}
