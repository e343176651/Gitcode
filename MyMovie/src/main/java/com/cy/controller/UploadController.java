package com.cy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cy.entity.Movie;
import com.cy.service.MovieUploadService;
import com.cy.vo.JsonResult;
import com.cy.vo.PageObject;

@Controller
@RequestMapping("/movie/")
public class UploadController {

	
	@Autowired
	private MovieUploadService MUS;
	
	@RequestMapping("doDeleteMovie")
	@ResponseBody
	public JsonResult doDeleteMovie(Movie entity) {
		MUS.deleteMovie(entity);
		return new JsonResult("删除成功!");
		
	}
	
	@RequestMapping("movieUpload")
	public String movieUpload() {
		
		return "movie_upload";
	}
	
	@RequestMapping("movie_list")
	public String movieList() {
		
		return "movie_list";
	}
	
	@RequestMapping("insertMovie")
	@ResponseBody
	public JsonResult insertMovie(Movie entity) {
		
		MUS.uploadMovie(entity);
		
		return new JsonResult("添加成功!");
		
	}
	
	@RequestMapping("findAllMovie")
	@ResponseBody
	
	public JsonResult findAllMovie(Integer pageCurrent){
		PageObject<Movie> po = MUS.findAllMovie(pageCurrent);
		
		return new JsonResult(po);
		
	}
	
	@RequestMapping("findObjectById")
	@ResponseBody
	
		public JsonResult findObjectById(Integer mId) {
		Movie movie = MUS.findObjectById(mId);
			return new JsonResult(movie);
		
	}
}
