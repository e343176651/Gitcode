package com.cy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cy.entity.SysUser;
import com.cy.service.SysUserService;
import com.cy.vo.JsonResult;
import com.cy.vo.PageObject;

@RestController
@RequestMapping("/user")
public class SysUserController {
	@Autowired
	private SysUserService sysUserService;
	@RequestMapping("doFindAllPageObjects")
	public JsonResult doFindAllPageObjects() {
		return new JsonResult(sysUserService.findAllPageObjects());
	
		
	}
	@RequestMapping("doFindPageObjects")
	public JsonResult doFindPageObjects(String username,Integer pageCurrent) {
		PageObject<SysUser> pageObject=sysUserService.findPageObjects(username, pageCurrent);
		return new JsonResult(pageObject);
		
	}
	@RequestMapping("doUpdatePassword")
	 public JsonResult doUpdatePassword(
		 String password) {
	 sysUserService.updatePassword(password);
	 return new JsonResult("update ok");
}
	@RequestMapping("doRegistUser")
	public JsonResult doRegistUser(SysUser sysUser) {
		sysUserService.registUser(sysUser);
		return new JsonResult("regist ok");
	}
}
