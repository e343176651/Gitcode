package com.cy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cy.entity.SysAdv;
import com.cy.pj.common.vo.JsonResult;
import com.cy.service.SysAdvService;
import com.cy.vo.PageObject;
import com.fasterxml.jackson.core.JsonProcessingException;
@RestController
@RequestMapping("/adv/")
public class SysAdvController {
	@Autowired
	private SysAdvService sysAdvService;
	
	@RequestMapping("doInsertObject")
	public JsonResult doInsertObject(
			SysAdv entity) {
		sysAdvService.insertObject(entity);
		 return new JsonResult("save ok");
	}
	
	@RequestMapping(value="doDeleteObjects",method = RequestMethod.POST)
	public JsonResult doDeleteObjects(
			Integer...adviceIds) {
		sysAdvService.deleteObjects(adviceIds);
		return new JsonResult("delete ok");
	}
	
	
	/**
	 * 基于请求参数执行日志信息的获取
	 * @param name 用户名
	 * @param pageCurrent 当前页码值
	 * @return 返回值为json格式的字符串
	 * 说明:假如对方法参数有更高要求可以
	 * 使用@RequestParam注解对参数进行描述
	 */
	@RequestMapping("doFindPageObjects")
	public JsonResult doFindPageObjects(
		    String name,
			Integer pageCurrent) throws JsonProcessingException {
	  	    System.out.println("======");
		    PageObject<SysAdv> pageObject=
	  	    sysAdvService.findPageObjects(name, pageCurrent);
		return  new JsonResult(pageObject);
	}
}








