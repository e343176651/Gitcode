package com.cy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cy.entity.Order;
import com.cy.service.OrderService;
import com.cy.vo.JsonResult;
import com.cy.vo.PageObject;

@Controller
@RequestMapping("/order/")
public class OrderController {

	@Autowired
	private OrderService os;
	
	@RequestMapping("doFindOrder")
	@ResponseBody
	public JsonResult doFindOrder(Integer uid,
			Integer pageCurrent) {
		PageObject<Order> pageObject=
		os.findOrder(uid, pageCurrent);
		return new JsonResult(pageObject);
	}
}
