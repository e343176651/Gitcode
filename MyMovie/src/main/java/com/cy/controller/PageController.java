 package com.cy.controller;


import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * 基于此Controller处理对象项目中所有页面请求
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/")
public class PageController {

	
	/**返回首页*/
	@RequestMapping("indexUI")
	public String indexUI() {
	
 
		return "index";
	}
	/**返回首页*/
	@RequestMapping("starterUI")
	public String starterUI() {
		return "pages/starter";
	}	
	@RequestMapping("doPag2eUI")
	public String doPageUI() {
		return "pages/common/page";
	}
	
	  @RequestMapping("{moduleUI}") 
	  public String doModuleUI(@PathVariable String moduleUI) { 
		  return moduleUI; 
		  }
	 
	
	  /**
	   * 返回后台用户管理模块页面
	   * @return
	   */
	  @RequestMapping("user_list")
		public String UserUI() {
			return "user_list";
		}
	/**
	 * 返回登录页面
	 * @return
	 */
	@RequestMapping("login") 
	public String loginUI() {
	 
	 return "login"; 
	 }
	 
	
	 @RequestMapping("book.jsp") 
	 public String bookUI() {
	  
	  return "book"; 
	  }
	 
	 /**返回投诉建议页面*/
	 @RequestMapping("adv_list") 
		public String advUI() {
		 return "adv_list"; 
		 }

	 @RequestMapping("log_list5") 
	 public String hall() {
		 return "pages/sys/log_list5"; 
		 }
	
}
