package com.cy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cy.entity.MSchedule;
import com.cy.service.MScheduleService;
import com.cy.service.impl.MInfoHallServiceImpl;
import com.cy.vo.JsonResult;

@Controller
@RequestMapping("/MSchedule/")
public class MScheduleController {
	@Autowired
	private MScheduleService mScheduleService;
	@Autowired
	private MInfoHallServiceImpl mInfoHallService;
	
	@RequestMapping("log_edit2")
	public String a() {
		
		return "log_edit2";
	}
	@RequestMapping("findPageMSchedule")
	@ResponseBody
	public JsonResult findPageMSchedule(String mName,Integer pageCurrent) {
		return new JsonResult(mScheduleService.findPageMSchedule(mName, pageCurrent));
	}
	
	@RequestMapping("addMschedule")
	@ResponseBody
	public JsonResult addMschedule(MSchedule mSchedule) {
		mScheduleService.addSchedule(mSchedule);
		return new JsonResult("add ok");
	}
	@RequestMapping("findCheckBox")
	@ResponseBody
	public JsonResult findCheckBox() {
		return new JsonResult(mInfoHallService.ChecBox());
	}
}
