package com.cy.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cy.pj.entity.User;
import com.cy.service.UserService;

import io.micrometer.core.instrument.util.StringUtils;

@Controller
@RequestMapping("/user/")
public class UserController {
    
	@Autowired
	private UserService userService;
	
	@RequestMapping("doSaveObject")
	public String doSaveObject(User entity){
		
		userService.saveObject(entity);
		
		return "login";
		
	}
	

	
	 @RequestMapping("dofindUser")
	   public String doFindUser(String username,String password){
		  
		 userService.findUser(username, password);
		 
		
		   return "index";
	   }
	 
	 
	 /**
	  * 登录跳转到首页时显示用户名
	  */
	 @RequestMapping(value="userLogin",method=RequestMethod.POST)
	 public String userLogin(HttpServletRequest request,HttpSession session) {
		 
		 String userName = request.getParameter("username");
		 String passWord = request.getParameter("password");
		 
		 if(!StringUtils.isBlank(userName) && !StringUtils.isBlank(passWord)) {			 			
		   User user=userService.findUser(userName, passWord);		   		
			 if(null!=user) {				 
				 session.setAttribute("sessionUser", user);
				 return "index";
			 }			 
		 }		 
		 return "login";		 
	 }
	 
	 
	 
	
}
