package com.cy.entity;

import lombok.Data;

@Data
public class Movie {
	
	
		private Integer mId;
		private String name;
		private String showDate;
		private String cast;
		private String direction;
		private String genre;
		private Integer duration;
		private String country;
		private String intro;
		private Integer price;


}




