package com.cy.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class MInfo {
	/**电影id*/
	private Integer id;
	/**电影名称*/
	private String name;
	/**演出时间*/
	private Date showDate;
	/**主演*/
	private String cast;
	/**导演*/
	private String direction;
	/**分类*/
	private String genre; 
	/**时长*/
	private Integer duration;
	/**国籍*/
	private String country;
	/**简历*/
	private String intro;
	/**价格*/
	private Double price;
}
