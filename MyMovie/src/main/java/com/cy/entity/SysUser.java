package com.cy.entity;

import lombok.Data;

@Data
public class SysUser {
	private Integer id;
	private String username;
	private String password;
	private String salt;
	private String email;
	private String phone;
	private String status;
	
}
