package com.cy.entity;

import java.io.Serializable;

import lombok.Data;

@Data
public class SysAdv implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5317049302528718257L;
	private Integer adviceId;
	private Integer uid;
	private String msg;
	private String name;
	private String email;
	private String phone;
}
