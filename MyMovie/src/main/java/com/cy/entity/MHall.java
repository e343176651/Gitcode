package com.cy.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class MHall {
	/**影厅id*/
	private Integer id;
	/**影厅名字*/
	private String hallName;
	/**行*/
	private Integer rows;
	/**列*/
	private Integer cols;
}
