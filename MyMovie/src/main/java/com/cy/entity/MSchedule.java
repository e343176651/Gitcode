package com.cy.entity;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class MSchedule implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2045490714064677677L;
	/**
	 * 演出计划id
	 */
	private Integer id;
	/**
	 * 演出厅id
	 */
	private MHall hallObj;
	/**
	 * 电影id
	 */
	private MInfo mObj;
	/**
	 * 上映日期
	 */
	private Date showDate;
	/**
	 * 开播日期
	 */
	private Date scheduleTime;
}
