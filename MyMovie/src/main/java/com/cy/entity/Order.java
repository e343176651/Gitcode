package com.cy.entity;

import lombok.Data;


@Data
public class Order {
	/**订单id*/
	private Integer oid;
	/**用户id*/
	private Integer uid;
	/**电影名*/
	private String  name;
	/**场次id*/
	private Integer sid;
	/**影厅id*/
	private Integer hallId;
	/**座位*/
	private String seat;
	/**票数*/
	private Integer count;
	/**总金额*/
	private Integer totalPrice;
	/**播放时间*/
	private String playTime;
	/**状态*/
	private String status;
	/**下单时间*/
	private String orderTime;
}
