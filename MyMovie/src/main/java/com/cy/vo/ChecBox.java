package com.cy.vo;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class ChecBox implements Serializable{
	private static final long serialVersionUID = -5755562250908116514L;
	private Integer id;
	private String name;
	private Date showDate;
	private Integer duration;
}
