package com.cy.vo;

import java.io.Serializable;
import lombok.Data;
import lombok.NoArgsConstructor;

/***
 * pojo-->vo
  *  基于此对象封装控制层要响应到客户端的数据
 */
@Data
@NoArgsConstructor
public class JsonResult implements Serializable{
	private static final long serialVersionUID = 677048178703375661L;
	/**状态码:1表示成功，0表示失败*/
    private int state=1;
    /**状态码对象的状态信息*/
    private String message="ok";
    /**用于接收业务层数据*/
    private Object data;
    public JsonResult(String message){
    	this.message=message;
    }
    public JsonResult(Object data){
    	this.data=data;
    }
    public JsonResult(Throwable e){
    	this.state=0;
    	this.message=e.getMessage();
    }
}