package com.cy.pj.common.web;

import org.springframework.web.bind.annotation.ControllerAdvice;
/**全局异常处理类*/
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cy.vo.JsonResult;

@ControllerAdvice
public class GlobalExceptionHandler1 {
	/**
	 * ExceptionHandler 
	 */
	@ExceptionHandler(RuntimeException.class)
	@ResponseBody
	public JsonResult doHandleRuntimeException(RuntimeException e) {
		//输出异常信息
		e.printStackTrace();
		//封装异常栈的信息
		return new JsonResult(e);
		
	}
	
}
