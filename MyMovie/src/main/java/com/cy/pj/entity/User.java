package com.cy.pj.entity;

import java.io.Serializable;

import lombok.Data;
/**
 * 用户注册信息封装类
 * @author Administrator
 *
 */
@Data
public class User implements Serializable{
     
	/**
	 * 
	 */
	private static final long serialVersionUID = 6941634016403467188L;
	private Integer id;
	/**用户名*/
	private String username;
	/**密码*/
	private String password;	
	/**邮箱*/
	private String email;
	/**手机号码*/
	private String phone;
	
	
	
	
}
