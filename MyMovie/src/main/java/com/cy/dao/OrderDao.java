package com.cy.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.cy.entity.Order;

@Mapper
public interface OrderDao {
		
		List<Order> findOrder(
		           @Param("uid")Integer uid,
		           @Param("startIndex")Integer startIndex,
		           @Param("pageSize")Integer pageSize);
		
		int getRowCount(@Param("uid")Integer uid);
}
