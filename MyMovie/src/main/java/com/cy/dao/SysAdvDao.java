package com.cy.dao;

import java.util.List;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.cy.entity.SysAdv;
@Mapper
public interface SysAdvDao {
	int insertObject(SysAdv entity);
	
	/**
	 * 基于记录id执行删除业务
	 * @param adviceIds 
	 * @return 删除行数
	 */
	 int deleteObjects(@Param("adviceIds")Integer...adviceIds);
	 
	 
	/**
	 * 基于条件统计用户评论建议
	 * @param name
	 * @return
	 */
	int getRowCount(@Param("name")String name);
	
	/**
	 * 基于条件uid,从指定位置startIndex,查询pageSize条数据
	 * @param name 用户名字
	 * @param startIndex 当前页起始位置
	 * @param pageSize 页面大小(每页最多显示多少条记录)
	 * @return 查询到的结果,一行记录对应一个SysAdv对象
	 */
	List<SysAdv> findPageObjects(
			@Param("name")String name,
			@Param("startIndex")Integer startIndex,
			@Param("pageSize")Integer pageSize
			);

	
}
