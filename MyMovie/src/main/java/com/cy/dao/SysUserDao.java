package com.cy.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.cy.entity.SysUser;
@Mapper
public interface SysUserDao {
	@Select("select * from users")
	List<SysUser> findAllPageObjects();
	
	int getRowCount(@Param("username") String username);
	
	List<SysUser> findPageObjects(
			 @Param("username")String  username,
		      @Param("startIndex")Integer startIndex,
		      @Param("pageSize")Integer pageSize);
	
	int updatePassword(
			@Param("password") String password,
			@Param("salt") String salt,
			@Param("id") Integer id);
	
	int registUser(SysUser sysUser);
}
