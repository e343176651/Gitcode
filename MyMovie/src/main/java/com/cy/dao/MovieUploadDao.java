package com.cy.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.cy.entity.Movie;

@Mapper
public interface MovieUploadDao {

	int uploadMovie(Movie entity);
	
	int updateMovie(Movie entity);
	
	List<Movie> findAllMovie(Integer startIndex, Integer pageSize);

	int getRowCount();
	
	Movie findObjectById(Integer mId);
	
	int deleteMovie(Movie entity);
	
	
}
