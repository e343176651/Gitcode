package com.cy.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.cy.entity.MSchedule;

@Mapper
public interface MScheduleDao {
	/**查询所有场次信息*/
	List<MSchedule> queryMscheduleList();
	/**查询总数*/
	int getRowCount(@Param("mName")String mName);
	/**分页查询所有场次信息*/
	List<MSchedule> findPageMSchedule(
			@Param("mName") String mName,
			@Param("startIndex")Integer startIndex,
			@Param("pageSize")Integer pageSize);
	
	int insertMSchedule(MSchedule mSchedule);
}
