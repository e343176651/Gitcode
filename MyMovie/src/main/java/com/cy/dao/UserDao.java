package com.cy.dao;

import org.apache.ibatis.annotations.Mapper;


import com.cy.pj.entity.User;
@Mapper
public interface UserDao {

	/**
	 * 负责将用户信息添加到数据库
	 */
	
	int insertObject(User entity);
	
	/**
	 * 登录时比对用户名和密码
	 * @param username
	 * @return
	 */
	User findUser(String username,String password);
	
	
}
	