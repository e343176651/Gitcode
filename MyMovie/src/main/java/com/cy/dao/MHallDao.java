package com.cy.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.cy.entity.MHall;

@Mapper
public interface MHallDao {
	/**查询某个影厅*/
	MHall getMHallById(Integer hallid);
	
	/**查询所有影厅id+name*/
	List<MHall> findMHall();
}
