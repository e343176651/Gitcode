package com.cy.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.cy.entity.MInfo;
import com.cy.vo.ChecBox;

@Mapper
public interface MInfoDao {
	/**查询某个电影*/
	MInfo getMinfoById(Integer mId); 
	/**查询所有电影id+name*/
	List<ChecBox> findMInfo();
}
