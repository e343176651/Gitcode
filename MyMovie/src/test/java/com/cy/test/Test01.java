package com.cy.test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.cy.controller.UploadController;
import com.cy.dao.MovieUploadDao;
import com.cy.dao.OrderDao;
import com.cy.entity.Movie;
import com.cy.service.MovieUploadService;

@SpringBootTest
public class Test01 {
@Autowired
	private OrderDao orderDao;

@Autowired
 	MovieUploadDao dao;
@Autowired
	MovieUploadService mus;
@Autowired
	UploadController uc;
	@Test
	public void test1() {
		System.out.println(orderDao.findOrder(1, 0, 2));
	}
	@Test
	public void test2() {
		System.out.println(mus.findAllMovie(1));
	}
	
	@Test 
	public void test3() {
		Movie m = new Movie();
		m.setMId(27);
		m.setName("f123123ff");
		m.setShowDate("2088-08-08 11:11:11");
		m.setGenre("asd");
		m.setCast("castt");
		m.setDirection("daoyan");
		m.setDuration(111);
		m.setCountry("美国");
		m.setIntro("简介简介简介简介简介简介简介简介");
		m.setPrice(66);
		System.out.println(uc.insertMovie(m));
	}
	
	@Test 
	public void test4() {
		Movie m = new Movie();
		m.setMId(27);
		int a = mus.deleteMovie(m);
		System.out.println(a);
	}
}
//org.junit.jupiter.api.extension.ParameterResolutionException: No ParameterResolver registered for parameter [com.cy.entity.Movie m] in method [public void com.cy.test.Test01.test3(com.cy.entity.Movie)].

